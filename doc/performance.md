## Performance

- Reasonably small:
  * The Rosie compiler/runtime/libraries take up less than 2MB on disk <!-- 
  du -ch /usr/local/lib/librosie.a /usr/local/bin/rosie /usr/local/lib/rosie/lib /usr/local/lib/rosie/rpl 
  -->
  * Memory usage by the CLI is currently excessive, but this [will improve](#memory-consumption) soon
- Good performance: 
  * Faster than [Grok](https://www.elastic.co/guide/en/logstash/current/plugins-filters-grok.html)
  (by a [factor of 4](images/perf_new.png) or more)
  * Slower than Unix [grep](https://en.wikipedia.org/wiki/Grep)
  * But Rosie does more than both of them
- Extensible pattern library
- Rosie is fluent in Unicode (UTF-8), ASCII, and the
  [binary language of moisture vaporators](http://www.starwars.com/databank/moisture-vaporator)
  (arbitrary byte-encoded data)


### Speed

A measurement we use in regression testing for performance is how long it takes
to process 1 million syslog entries using the pattern `syslog` as defined here:

```
-- syslog.rpl

import date, time, net, word, num
~ = [:space:]+

message = .*
datetime = { date.rfc3339 "T" time.rfc3339 }
syslog = datetime net.ipv4 {word.any "["num.int"]:"} message
``` 

The current release, v1.1.0, performs as follows:

Number of lines | Output format | Seconds (user) | Lines/sec         | Machine
--------------- | ------------- | -------------- | ----------------- | -------
1,000,000       | binary        | 5.8            | 172,000 (approx.) | MacBook Pro 2.9 GHz Intel Core i5
1,000,000       | JSON          | 11.7           |  85,500 (approx.) | MacBook Pro 2.9 GHz Intel Core i5

Notes:
- The binary output format encodes the pattern type and start/end positions of
  the match.
- The JSON output format includes the pattern type, start/end positions, and a
  copy of the data (input) that matched.
- These are measurements of the Rosie CLI, which is single-threaded. 


### Memory consumption

The current release, v1.1.0, consumes memory egregiously (in the range 64MB --
250Mb).  Our roadmap includes reducing this number considerably.  A proof of
concept is the program
[match.c](https://gitlab.com/rosie-pattern-language/rosie/blob/dev/src/rpeg/test/match.c),
which requires less than 1MB.  This small command-line program is linked only
with the Rosie run-time.  It needs an RPL pattern that has been compiled in
advance, which is a prototype feature that has not yet been released.

This proof of concept needs around 9.0 seconds (user time) to execute the syslog
test mentioned above, generating JSON output.  This rate, about 110k lines/sec,
suggests that current Rosie CLI has significant unnecessary overhead.  

We hypothesize that much of the memory and speed overhead of the current CLI
derives from the file processing loop, which is written in Lua.  And Lua
[interns strings](https://en.wikipedia.org/wiki/String_interning). The `match.c`
prototype does the same work, reading input line by line and matching against a
compiled pattern, but it is written in pure C.


