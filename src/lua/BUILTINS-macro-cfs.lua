--------------------------------------------------------------------------------
-- Simple Case Folding
--------------------------------------------------------------------------------
local cfs = {}

local ci = import "BUILTINS-macro-ci"
local cff = import "BUILTINS-macro-cff"
local unicode_util = import "unicode-util"

local simple_case_mapper = import "UNICODE-reverse-case-folding-table-CS"
local C_case_mapper      = import "UNICODE-case-folding-table-C"
local S_case_mapper      = import "UNICODE-case-folding-table-S"


local function get_simple_cases(char)
    local cases = {char}
    local codepoint_decimal = utf8.codepoint(char)
    local codepoint_hex = string.format("%04X", codepoint_decimal)
    local case_map = simple_case_mapper[codepoint_hex]

    if (case_map ~= nil) then
        for key_hex, _ in pairs(case_map) do
            local key_decimal = tonumber(key_hex, 16)

            cases[#cases + 1] = utf8.char(key_decimal)

        end
    end

    return cases
end


-- The argument, char, MUST be a string containing one UTF-8 encoded character
-- or a single byte (which will be in 0x80-0xFF).
local function generate_cfs_expression_for_char(char, sref)
    -- A literal is stored in its escaped format, so we must escape it as we
    -- create literals.  TODO: Do not store literals in escaped format.
    -- fetch_cases_for_char(char) will index into large file and return a list
    -- (really a Lua "array") of 1 or more strings, each of which is a valid case
    -- for the char argument.

    local cases = get_simple_cases(char)
    local escaped_cases = list.map(ustring.escape_string, cases)
    local char_literals = list.map(
            function(escaped_case)
                return ast.literal.new{value=escaped_case, sourceref=sref}
             end, escaped_cases
    )

    if #cases == 1 then
       return char_literals[1]    -- no "choice" structure needed
    else
       return ast.choice.new{exps=char_literals, sourceref=sref}
    end
end


local function to_cfs_literal(exp)
    local simple_case_folded_value = unicode_util.case_fold(exp.value, C_case_mapper)
    simple_case_folded_value = unicode_util.case_fold(simple_case_folded_value, S_case_mapper)

    local input = ustring.explode(simple_case_folded_value)
    local disjunctions = list.new()
    for _, char in ipairs(input) do
      table.insert(disjunctions, generate_cfs_expression_for_char(char, exp.sourceref))
    end -- for each char in the input literal
    return ast.raw.new{
        exp = ast.sequence.new{
            exps      = disjunctions,
            sourceref = exp.sourceref
        },
        sourceref=exp.sourceref
    }
end


local function to_range_or_char(r, exp)
   if r[1] == r[2] then
      -- list of one char (could use a literal instead)
      return ast.cs_list.new{ complement = false,
                  chars = { r[1] },
                  sourceref = exp.sourceref }
   end
   return ast.cs_range.new{ complement = false,
                first = r[1],
                last = r[2],
                sourceref = exp.sourceref }
end


-- to_case_insensitive: named charsets
function cfs.to_ci_named_charset(exp)
   local other_case
   if exp.name == 'upper' then
      other_case = 'lower'
   elseif exp.name == 'lower' then
      other_case = 'upper'
   end
   if other_case then
      local upper_and_lower =
     {exp,
      ast.cs_named.new{complement = exp.complement,
               name = other_case,
               sourceref = exp.sourceref}}
      return ast.choice.new{exps = upper_and_lower, sourceref = exp.sourceref}
   end
   -- else we have a named set that has no other case
   return exp
end


-- to_case_insensitive: character lists
-- FUTURE: flatten the choices?
function cfs.to_ci_list_charset(exp)
   local disjunctions = list.new()
   for _, char in ipairs(exp.chars) do
      table.insert(disjunctions, generate_ci_expression_for_char(char, exp.sourceref))
   end -- for each char in the input literal
   return ast.raw.new{exp=ast.choice.new{exps=disjunctions, sourceref=exp.sourceref},
              sourceref=exp.sourceref}
end


-- to_case_insensitive: character ranges
function cfs.to_ci_range_charset(exp)
   local _, alternate_case_ranges = ustring.cased_subranges(exp.first, exp.last)
   local ci_ranges = list.map(function(r) return to_range_or_char(r, exp) end,
                  alternate_case_ranges)
   local exps = { ast.cs_range.new{ complement = false,
                    first = exp.first,
                    last = exp.last,
                    sourceref = exp.sourceref } }
   for _, exp in ipairs(ci_ranges) do
      table.insert(exps, exp)
   end
   return ast.bracket.new{ complement = exp.complement,
               cexp = ast.choice.new{ exps = exps, sourceref = exp.sourceref },
               sourceref = exp.sourceref }
end


-- The ci macro is UTF8-aware but only converts the case of ASCII letter characters.
function cfs.macro_case_fold_simple(...)
   local args = {...}
   if #args~=1 then error("cfs takes one argument, " .. tostring(#args) .. " given"); end
   local exp = args[1]
   local retval = ast.visit_expressions(exp, ast.literal.is, to_cfs_literal)
   retval = ast.visit_expressions(retval, ast.cs_named.is, ci.to_ci_named_charset)
   retval = ast.visit_expressions(retval, ast.cs_list.is, ci.to_ci_list_charset)
   retval = ast.visit_expressions(retval, ast.cs_range.is, ci.to_ci_range_charset)
   return retval
end


return cfs
