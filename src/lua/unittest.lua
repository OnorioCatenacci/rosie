-- -*- Mode: Lua; -*-                                                                             
--
-- unittest.lua      Implements the 'test' command of the cli
--
-- © Copyright IBM Corporation 2017, 2018, 2019.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHORS: Jamie A. Jennings, Kevin Zander

-- This code badly needs refactoring!

local unittest = {}

local cli_common = import("cli-common")
local io = import("io")
local common = import("common")
local ustring = import("ustring")
local violation = import("violation")

local write_error = function(...) io.stderr:write(...) end

local function startswith(str,sub)
  return string.sub(str,1,string.len(sub))==sub
end

local split = util.split

local function find_test_lines(str)
  local lines = {}
  local line_numbers = {}
  for i, line in pairs(split(str, "\n")) do
     if startswith(line,'-- test') or startswith(line,'--test') then
        table.insert(lines, line)
	table.insert(line_numbers, i)
     end
  end
  return lines, line_numbers
end

-- Create a fresh engine and load it up with patterns for parsing out all the
-- rpl unit test cases in an rpl file.  Return the rplx that does this.
function unittest.setup(en, rpl_definition_file)
   local test_patterns =
      [==[
	 identifier = rpl.identifier           -- rename
	 quoted_string = rpl.quoted_string
	 includesKeyword = "includes" / "excludes"
	 includesClause = includesKeyword identifier
	 testKeyword = "accepts" / "rejects"
	 test_local = "local"
	 test_line = ("--test" / "-- test")
	             test_local?
	             identifier 
	             (testKeyword / includesClause) 
		     (quoted_string ("," quoted_string)*)?
     ]==]
   local ok, pkgname, errs = en:import(rpl_definition_file, "rpl")
   if not ok then
      for _,v in ipairs(errs or {}) do
	 write_error(violation.tostring(v))
      end
      error("Internal error")
   end
   local ok = en:load(test_patterns)
   assert(ok, "internal error: test_patterns failed to load")
   local test_rplx, errs = en:compile("test_line")
   if not test_rplx then
      errs = table.concat(map(violation.tostring, errs), "\n");
      assert(test_rplx, "internal error: test_line failed to compile: " .. errs)
   end
   return test_rplx
end   

local function shorten(str, len)
   if #str > len then
      return "..." .. str:sub(#str-len+4)
   end
   return str
end

local function indent(str, col)
   return str:gsub('\n', '\n' .. string.rep(" ", col))
end

local right_column = 2

function unittest.write_test_result(...)
  io.write(string.rep(" ", right_column))
  for _,item in ipairs{...} do io.write(item); end
  io.write("\n")
end

local function extract_test_string(p)
   assert(p.type == "quoted_string")
   assert(p.subs and p.subs[1].type == "rpl.literal")
   local data = p.subs[1].data
   local teststr, esc_err = ustring.unescape_string(data)
   if not teststr then
      return false, esc_err or "test input is not a valid Rosie string", "error"
   end
   return teststr
end

local function make_compilation_error(exp, message)
   return "ERROR: " .. message .. ": " .. tostring(exp)
end

-- Recursively search the given list of submatches for the first one where the
-- type is t.  (This is depth-first search returning only a boolean.)
local function search_subs(subs, t)
   if not subs then return false; end
   for _, s in ipairs(subs) do
      if s.type == t then
	 return true
      elseif s.subs then
	 if search_subs(s.subs, t) then
	    return true
	 end
      end
   end -- for each sub
   return false
end

-- Return value:
--   true (includes id) or false (does not include id) or "error" (did not match)
local function test_includes(rplx, input, id)
  local result, leftover = rplx:match(input)
  if result and (leftover==0) then
     return search_subs(result.subs, id), nil
  end
  return "error"
end

function unittest.make_test_engine(rosie, args, filename)
   -- fresh engine for testing this file
   local test_engine = rosie.engine.new()
   -- set it up using whatever rpl strings or files were given on the command line
   cli_common.setup_engine(rosie, test_engine, args)
   -- load the rpl code we are going to test (second arg true means "do not
   -- search LIBPATH for file")
   local ok, pkgname, errs, actual_path
   if filename then
      ok, pkgname, errs, actual_path = test_engine:loadfile(filename, true)
      if not ok then
	 local msgs = list.map(violation.tostring, errs)
	 local msg = table.concat(msgs, "\n")
	 return false, msg
      end
      return test_engine, pkgname
   end
   -- no filename to load
   return test_engine, nil
end
   
-- Read the tests out of the file
function unittest.extract_tests_from_file(filename)
   local f, msg = io.open(filename, 'r')
   if not f then error(msg); end
   local str = f:read('*a')
   if not str then
      f:close()
      error("could not read file: " .. filename)
   end
   local test_lines, line_numbers = find_test_lines(str)
   f:close()
   return test_lines, line_numbers
end

local left_delim = utf8.char(0x300A)
local right_delim = utf8.char(0x300B)

-- This replaces ustring.requote() so that the user can see exactly what is the
-- Rosie interpretation of the string they are unit testing.
local function printable(str)
   return left_delim .. str .. right_delim
end   

-- Do tests of inclusion/exclusion
function unittest.do_include_test(rplx, testIdentifier, testInfo, input)
   assert(testInfo.subs and testInfo.subs[1] and testInfo.subs[1].type=="includesKeyword")
   local testing_excludes = (testInfo.subs[1].data=="excludes")
   assert(testInfo.subs[2] and testInfo.subs[2].type=="identifier",
	  "not an identifier: " .. tostring(testInfo.subs[2].type))
   local containedIdentifier = testInfo.subs[2].data
   local status, teststr, msg
   teststr, msg = extract_test_string(input)
   if not teststr then
      return 1, 0, 0, 0, "ERROR: " .. msg
   end
   status, msg = test_includes(rplx, teststr, containedIdentifier)
   if status=="error" then
      return 0, 0, 1, 0, "BLOCKED: " .. testIdentifier .. " did not accept " .. printable(teststr)
   elseif status==false then
      if (not testing_excludes) then
	 msg = testIdentifier .. " did not include " .. containedIdentifier ..
	    " with input " .. printable(teststr)
	 return 0, 1, 0, 0, "FAILED: " .. msg
      else
	 return 0, 0, 0, 1, nil			    -- pass
      end
   elseif status==true then
      if testing_excludes then
	 msg = testIdentifier .. " did not exclude " .. containedIdentifier ..
	    " with input " .. printable(teststr)
	 return 0, 1, 0, 0, "FAILED: " .. msg
      else
	 return 0, 0, 0, 1, nil			    -- pass
      end
   else
      error("internal error: unexpected test status: " .. tostring(status))
   end
end

function unittest.do_match_test(rplx, testIdentifier, testInfo, input)
   local should_accept = (testInfo.data == "accepts")
   local teststr, msg = extract_test_string(input)
   if not teststr then
      return 1, 0, 0, 0, "ERROR: " .. msg
   else
      local result, leftover = rplx:match(teststr)
      if #teststr==0 then teststr = "the empty string"; end -- for display purposes
      local pass
      if should_accept then
	 pass = (result and (leftover==0))
      else
	 pass = (not result) or (leftover~=0)
      end
      if pass then
	 return 0, 0, 0, 1, nil		            -- pass
      else
	 msg = "FAILED: " .. testIdentifier .. " did not "
	    .. testInfo.data:sub(1,-2).. " ".. printable(teststr)
	 return 0, 1, 0, 0, msg			    -- fail
      end
   end -- if teststr is a valid Rosie string
end

function unittest.lookup(test_engine, identifier, pkgname)
  local rplx = test_engine:lookup(identifier, pkgname, false)
  if rplx then
     return rplx, "exported"
  else
     rplx = test_engine:lookup(identifier, pkgname, true)
     if rplx then
	return rplx, "local"
     else
	return nil, "error"
     end
  end
end

-- do_test() is called after we know that we can do tests because the identifier
-- has compiled and is accessible (local or exported, as the test requires).
-- Return values: errors, failures, blocked, passed, maybe message
function unittest.do_test(rplx, testIdentifier, testInfo, input)
   local testType = testInfo.type
   if testType == "includesClause" then
      return unittest.do_include_test(rplx, testIdentifier, testInfo, input)
   elseif testType == "testKeyword" then
      -- test accepts/rejects
      return unittest.do_match_test(rplx, testIdentifier, testInfo, input)
   else -- unknown test type
      error("internal error: parser for test expressions produced unexpected test type: " .. tostring(testType))
   end
end 

-- Return values: errors, failures, blocked, passed, total, message table
function unittest.do_line(test_rplx, line, test_engine, pkgname)
   -- Extract all the tests on this line
   local m, left = test_rplx:match(line)
   -- Literals to test against will be in subs at offset 3 (this is based on the
   -- definition of 'test_line' in unittest.setup()).
   local literals = 3
   if not m then
      return 1, 0, 0, 0, 1, {"ERROR: invalid test syntax: " .. line}
   end
   local is_local_identifier = (m.subs[1].type=="test_local")
   if is_local_identifier then
      table.remove(m.subs, 1)
   end
   local testIdentifier = m.subs[1].data
   local testInfo = m.subs[2]
   if #m.subs < literals then
      return 1, 0, 0, 0, 1,
         {"ERROR: invalid test syntax (missing quoted input strings): " .. line}
   end
   -- At this point, m.subs[literals, ...] is a list of input strings to test 
   local total = #m.subs - literals + 1
   -- First make sure the identifier is bound and is local/exported as needed
   local rplx, where = unittest.lookup(test_engine, testIdentifier, pkgname)
   local block_msg = "BLOCKED: " .. tostring(total) ..
		     (((total==1) and " test of ") or " tests of ") .. testIdentifier
   if where=="error" then
      return 1, 0, total, 0, total, {
	 make_compilation_error(testIdentifier,
				"pattern did not compile (or is not defined)"),
	 block_msg
      }
   else
      if is_local_identifier and (where=="exported") then
	 -- User wants to test a local, but identifier is exported	 
	 return 1, 0, total, 0, total, {
	    make_compilation_error(testIdentifier,
				   "pattern is exported; use 'test' not 'test local'"),
	    block_msg
	 }
      elseif (not is_local_identifier) and (where=="local") then
	 -- User wants to test an exported identifier, but it is a local
	 return 1, 0, total, 0, total, {
	    make_compilation_error(testIdentifier,
				   "pattern is local; use 'test local' instead"),
	    block_msg
	 }
      end
   end
   -- ASSERT: no errors, and no mismatch between exported/local
   local messages = {}
   local errors, failures, blocked, passed, total = 0, 0, 0, 0, 0
   for i = literals, #m.subs do
      local ee, ff, bb, pp, msg = unittest.do_test(rplx,
						   testIdentifier,
						   testInfo,
						   m.subs[i])
      errors = errors + ee
      failures = failures + ff
      blocked = blocked + bb
      passed = passed + pp
      total = total + 1
      if msg then table.insert(messages, msg) end
   end -- for each input string to test
   return errors, failures, blocked, passed, total, messages
end   


-- Args to run():
--   rosie, the rosie module (for creating a new engine, and rplx objects for en)
--   en, the engine that has patterns for extracting test cases
--   args, the CLI args needed to set up the test engine
--   filename, the rpl file to load (compile) and which has the tests
-- Return values:
--   true if no execution errors, and if that is the case, then also:
--   number of test errors, failures, blocked, passed, and total
function unittest.run(rosie, test_rplx, args, filename)
   io.stdout:write(filename, '\n')
   local test_engine, pkgname = unittest.make_test_engine(rosie, args, filename)
   if not test_engine then
      io.write(indent(pkgname, right_column), "\n") -- error message
      return false
   end
   if args.verbose then
      unittest.write_test_result("File compiled successfully")
   end
   local test_lines, line_numbers = unittest.extract_tests_from_file(filename)
   if #test_lines == 0 then
      unittest.write_test_result("No tests found")
      return true
   end
   local failures, errors, blocked, passed, total = 0, 0, 0, 0, 0
   for i, line in pairs(test_lines) do
      local ee, ff, bb, pp, tt, msgs = unittest.do_line(test_rplx, line, test_engine, pkgname)
      errors = errors + ee
      failures = failures + ff
      blocked = blocked + bb
      passed = passed + pp
      total = total + tt
      local line_info = string.format("line %-3d ", line_numbers[i])
      for _, msg in ipairs(msgs) do unittest.write_test_result("    ", line_info, msg) end
   end -- for each line of tests
   if failures == 0 and errors == 0 and blocked == 0 then
      unittest.write_test_result("All ", tostring(total), " tests passed")
   else
      unittest.write_test_result("Tests: ", tostring(total),
				 "  Errors: ", tostring(errors),
				 "  Failures: ", tostring(failures),
				 "  Blocked: ", tostring(blocked),
				 "  Passed: ", tostring(passed))
   end
   return true, errors, failures, blocked, passed, total
end

return unittest

