-- -*- Mode: Lua; -*-                                                                             
--
-- trace.lua
--
-- © Copyright Jamie A. Jennings 2019, 2020.
-- © Copyright IBM Corporation 2017.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings


local ast = require "ast"
local builtins = require "builtins"
local common = require "common"
local pattern = common.pattern
local match = common.match
local json = require "cjson"

local trace = {}

local fn_BYTE_ENCODING, BYTE_ENCODING = common.lookup_encoder("byte")

-- Snapshot the match data.  This is needed because the match function returns a
-- pointer to a reusable string buffer.  Every time expression() is called,
-- another call to match() is made, which updates the buffer.  This is not a
-- problem generally in Rosie.  Here in the trace module, we used to pass around
-- raw return values from match(), which were userdata pointers.
local function snap(expected)
   if type(expected)=="userdata" then
      return lpeg.decode(expected) or error("lpeg.decode failed!")
   end
   return expected
end


---------------------------------------------------------------------------------------------------
-- Print a trace
---------------------------------------------------------------------------------------------------

local left_delim = utf8.char(0x300A)
local right_delim = utf8.char(0x300B)

local node_marker_string = utf8.char(0x251c) .. utf8.char(0x2500) .. utf8.char(0x2500) .. " "
local last_node_marker_string = utf8.char(0x2514) .. utf8.char(0x2500) .. utf8.char(0x2500) .. " "
local node_marker_len = utf8.len(node_marker_string)
assert(utf8.len(last_node_marker_string) == node_marker_len)

local tab_string = string.rep(" ", node_marker_len-1)
local vertical_bar = utf8.char(0x2502)

local function node_marker(is_last_node)
   if is_last_node == nil then return ""
   else return (is_last_node and last_node_marker_string or node_marker_string)
   end
end

local function tab(is_last_node)
   if is_last_node == nil then return ""
   else return (is_last_node and " " or vertical_bar) .. tab_string
   end
end

-- FUTURE:
-- + Include in the trace ALL the components of a sequence/choice
-- + Draw the tree using codepoints 2500-257F
-- - Compress a sequence of simple charset matches to one trace entry
-- - Print a statement about leftover characters after the tree
-- + Show the "best guess" as to what went wrong by focusing on the path in the trace (tree) that
--   went the furthest in the input.  (This is a heuristic.)
-- - Maybe add color to highlight where branches fail?
-- - Show sequences different from branching due to choices
-- - Make this [below] convention a configuration option with display settings like:
--   "none" - output the bytes in the input as-is
--   "hex" - translate each byte into its 2-char hex representation
--   "ascii" - translate 0-31 to ^@^A-^Z^[^\^]^^^_ and 127-255 into \x##
--   "utf8" - translate as follows
--                          utf8               utf8     
--     CHAR                 GLYPH              CODEPOINT    
--     Space                dot                U+00B7       
--     Tab                  right arrow        U+2192       
--     Newline              down arrow         U+2193
--     Return               solid bent arrow   U+21B5
--     Other whitespace     white dot          U+25E6 
--     Other non-printable  white box          U+25A1


-- 'node_tostrings' returns a table of strings, one per line of output.  The first time it is
-- called, the is_last_node argument should be nil.  Subsequent (recursive) calls will supply a
-- boolean value.

local function one_node_tostrings(t, is_last_node, in_seq)
   local t_ast_lines = util.split(ast.tostring(t.ast), '\n')
   assert(t_ast_lines[1])
   local lines = { node_marker(is_last_node, in_seq) .. "Expression: " .. t_ast_lines[1] }
   for i = 2, #t_ast_lines do
      table.insert(lines, tab(is_last_node, in_seq) .. t_ast_lines[i])
   end
   if t.match ~= nil then
      table.insert(lines,
		   tab(is_last_node, in_seq) ..
		   "Looking at: " .. left_delim .. t.input:sub(t.start) ..
		   right_delim .. " (input pos = " .. tostring(t.start) .. ")")
   end
   if t.match then
      local extra = ""
      if t.match and ast.grammar.is(t.ast) then
	 if t.match.subs and t.match.subs[1] then
	    extra = " (" .. t.match.subs[1].type .. ")"
	 end
      end
      table.insert(lines, tab(is_last_node, in_seq) ..
		   "Matched " .. tostring(t.nextpos - t.start) .. " chars" .. extra)
   elseif t.match == false then
      table.insert(lines, tab(is_last_node, in_seq) .. "No match")
   else
      assert(t.match==nil)
      table.insert(lines, tab(is_last_node, in_seq) .. "Not attempted")
   end
   return lines
end

local function select_subs_to_show(node, sub_on_path)
   assert(node.ast)
   if not sub_on_path then
      -- If we are not following a path, then we are showing the entire tree
      return node.subs or {}
   end
   local is_sequence = ast.sequence.is(node.ast)
   local subs = list.new()
   if (not node.subs) or not list.member(sub_on_path, list.from(node.subs)) then
      -- Trace trees are fuller than ASTs, i.e. they have sub-traces where identifiers are
      -- referenced and where quantified expressions are unrolled.  So there are times when we
      -- show a node but not its subs because the path did not include one of the subs.
      return subs, is_sequence
   elseif is_sequence or ast.choice.is(node.ast) then
      local i = 1
      while node.subs[i] do
	 table.insert(subs, node.subs[i])	    -- FUTURE: write list.insert?
	 i = i+1
      end
      return subs, is_sequence
   else
      return list.new(sub_on_path), is_sequence
   end
end

local function node_tostrings(t, is_last_node, path)
   local lines = one_node_tostrings(t, is_last_node)
   local sublines = {}
   if path and list.null(path) then
      return lines
   end
   local subs_to_show, in_sequence = select_subs_to_show(t, path and list.car(path))
   local next_path = path and list.cdr(path)
   local last = #subs_to_show
   for i = 1, last do
      local onesublines = node_tostrings(subs_to_show[i], (i==last), next_path)
      table.move(onesublines, 1, #onesublines, #sublines+1, sublines)      
   end
   for i = 1, #sublines do
      if is_last_node ~= nil then
	 sublines[i] = tab(is_last_node, in_sequence) .. sublines[i]
      end
   end
   table.move(sublines, 1, #sublines, #lines+1, lines)      
   return lines
end

function trace.tostring(t)
   assert(t.ast)
   local lines = node_tostrings(t)
   return table.concat(lines, "\n")
end
      
-- Return the trace leaf that has the larger value of nextpos, i.e. the leaf that consumed more of
-- the input string.
local function better_of(new_leaf, current_max)
   assert((not current_max) or current_max.nextpos)
   if (new_leaf and new_leaf.nextpos) then
      if (not current_max) then
	 return new_leaf
      elseif (new_leaf.nextpos > current_max.nextpos) then
	 return new_leaf
      end
   end
   return current_max
end

-- When a match fails, all of the leaves in the trace tree will indicate a match failure.  We
-- cannot be sure about which path through the tree is the one that the user *thought* would match
-- the input, but we can guess.  A reasonable guess is the path from the root to the leaf that
-- consumed the most input characters.  In the case of a tie, we will choose the path we encounter
-- first. 
local function max_leaf(t, max_node)
   if t.subs then
      for _, sub in ipairs(t.subs) do
	 local local_max = max_leaf(sub)
	 max_node = better_of(local_max, max_node)
	 sub.parent = t				    -- establish a path going UP the tree
      end
      assert(max_node)
      return max_node
   else
      return better_of(t, max_node)
   end
end

local function trim_matches(path)
   local last_match_index = 1
   for i = #path, 2, -1 do
      if not path[i].match then break; end
      last_match_index = i
   end
   for i = last_match_index + 1, #path do
      path[i] = nil
   end
   return path
end

function trace.max_path(t)
   local leaf = max_leaf(t)
   local path = list.new(leaf)
   while leaf.parent do
      path = list.cons(leaf.parent, path)
      leaf = leaf.parent
   end
   return path --trim_matches(path)
end

function trace.path_tostring(p)
   assert(p and p[1] and p[1].ast)
   local lines = node_tostrings(list.car(p), nil, list.cdr(p))
   return table.concat(lines, "\n")
end


-- Utility

-- See comment in parse_core.lua
reusable_output_buffer = lpeg.newbuffer()

local function protected_match(a, peg, input, start, parms)
--    print("*** In protected_match:")
--    print("a = " .. ast.tostring(a))
--    print("input = " .. input)
--    print("start = " .. start)
--    print("parms = " .. table.tostring(parms, false))
   local skippeg = ((start > 1) and P(start-1) * peg) or peg
   local wrapped_peg = common.match_node_wrap(skippeg, "*")
   local ok, m, leftover =
      pcall(match, wrapped_peg, input, 1,
	    BYTE_ENCODING, fn_BYTE_ENCODING, parms,
	    0, 0, reusable_output_buffer)
   m = snap(m)
--    print("ok = " .. tostring(ok))
--    print("match = " .. tostring(m) .. ", leftover = " .. tostring(leftover))
--    print()
   if not ok then
      print("\n\n\nTrace failed while working on: ", a)
      if a.exps then print("a.exps: " .. tostring(list.from(a.exps))); end
      print("ast.tostring(a) is: " .. ast.tostring(a))
      print("start is: " .. tostring(start) .. " and input is: |" ..  input .. "|")
      error("match failed: " .. m)		    -- m is error message from pcall
   end
   assert(type(m)=="table" or m==false, "m is " .. tostring(m))
   assert(type(leftover)=="number")
   -- Return 'false' for leftover if no match, to catch the error of depending
   -- upon leftover value when there was no match.
   return m, (m and leftover)
end

---------------------------------------------------------------------------------------------------
-- Trace functions for each expression type
---------------------------------------------------------------------------------------------------

local expression;

-- Append to 'matches' a trace record for each item in 'exps' that we didn't even try to match,
-- which are those beginning at index 'start'.
local function append_unattempted(exps, start, matches, input, nextstart)
   for i = start, #exps do
      table.insert(matches, {ast=exps[i], input=input, start=nextstart})
   end
end

local function sequence(e, a, input, start, expected, nextpos)
   local matches = {}
   local nextstart = start
   for _, exp in ipairs(a.exps) do
      local result = expression(e, exp, input, nextstart)
      table.insert(matches, result)
      if not result.match then break
      else nextstart = result.nextpos; end
   end -- for
   local n = #matches
   if n < #a.exps then
      append_unattempted(a.exps, n+1, matches, input, nextstart)
   end
   if (n==#a.exps) and (matches[n].match) then
      assert(expected, "sequence match differs from expected")
      assert(matches[n].nextpos==nextpos, "sequence nextpos differs from expected")
      return {match=snap(expected), nextpos=nextpos, ast=a, subs=matches, input=input, start=start}
   else
      assert(not expected, "sequence non-match differs from expected")
      return {match=snap(expected), nextpos=nextpos, ast=a, subs=matches, input=input, start=start}
   end
end

local function choice(e, a, input, start, expected, nextpos)
   local matches = {}
   for _, exp in ipairs(a.exps) do
      local result = expression(e, exp, input, start)
      table.insert(matches, result)
      if result.match then break; end
   end -- for
   local n = #matches
   if n < #a.exps then
      append_unattempted(a.exps, n+1, matches, input, matches[n].nextpos)
   end
   if expected~=nil then
      if matches[n].match then
	 assert(expected, "choice match differs from expected")
      else
	 assert(not expected, "choice non-match differs from expected")
      end
   end
   return {match=snap(expected), nextpos=nextpos, ast=a, subs=matches, input=input, start=start}
end

-- FUTURE: A qualified reference to a separately compiled module may not have an AST available
-- for debugging (unless it was compiled with debugging enabled).

local function ref(e, a, input, start, expected, nextpos)
   local pat = a.pat
   assert(pat.ast, "missing ast?")
   if pat.ast.sourceref == builtins.sourceref then
      -- In a trace, a reference no subs if it is built-in
      return {match=snap(expected), nextpos=nextpos, ast=a, input=input, start=start}
   else
      local result = expression(e, pat.ast, input, start)
      if expected then
	 assert(result.match, "reference match differs from expected")
	 assert(nextpos==result.nextpos, "reference nextpos differs from expected")
      else
	 assert(not result.match)
      end
      -- In a trace, a reference has one sub (or none, if it is built-in)
      return {match=snap(expected), nextpos=nextpos, ast=a, subs={result}, input=input, start=start}
   end
end

-- Note: 'atleast' implements * when a.min==0
local function atleast(e, a, input, start, expected, nextpos)
   local matches = {}
   local nextstart = start
   assert(type(a.min)=="number")
   while true do
      local result = expression(e, a.exp, input, nextstart)
      table.insert(matches, result)
      if not result.match then break
      else nextstart = result.nextpos; end
   end -- while
   local last = matches[#matches]
   if (#matches > a.min) or (#matches==a.min and last.match) then
      assert(expected, "atleast match differs from expected")
      assert(nextstart==nextpos, "atleast nextpos differs from expected")
      return {match=snap(expected), nextpos=nextpos, ast=a, subs=matches, input=input, start=start}
   else
      assert(not expected, "atleast non-match differs from expected")
      return {match=snap(expected), nextpos=nextpos, ast=a, subs=matches, input=input, start=start}
   end
end

-- 'atmost' always succeeds, because it matches from 0 to a.max copies of exp, and it stops trying
-- to match after it matches a.max times.
local function atmost(e, a, input, start, expected, nextpos)
   local matches = {}
   local nextstart = start
   assert(type(a.max)=="number")
   for i = 1, a.max do
      local result = expression(e, a.exp, input, nextstart)
      table.insert(matches, result)
      if not result.match then break
      else nextstart = result.nextpos; end
   end -- while
   local last = matches[#matches]
   assert(expected, "atmost match differs from expected")
   if last.match then
      assert(last.nextpos==nextpos, "atmost nextpos differs from expected")
   end
   return {match=snap(expected), nextpos=nextpos, ast=a, subs=matches, input=input, start=start}
end

local function bracket_explanation(a, input, start, m, complement)
   return (" " .. ast.tostring(a) ..
	   " Looking at input pos " .. tostring(start) .. ": " .. input:sub(start) .. 
           " And there " .. (m and "was" or "was NOT") .. " a match." ..
           " And complement is: " .. tostring(complement))
end

local function cs_simple(e, a, input, start, expected, nextpos)
--   print("*** In cs_simple, a = " .. ast.tostring(a) .. " expected = " .. tostring(expected))
   local complement = a.complement
   local simple = a.pat
   assert(pattern.is(simple))
   local m, leftover = protected_match(a,
				       simple.peg,
				       input,
				       start,
				       common.attribute_table_to_table(e.encoder_parms))
   if expected ~= nil then
      if (m and (not complement)) then
	 assert(expected, "simple character set match differs from expected: " ..
		bracket_explanation(a, input, start, m, complement))
      elseif (not m) and complement then
	 assert(not expected, "simple character set non-match differs from expected: " ..
		bracket_explanation(a, input, start, m, complement))
      end
   end -- if there is an expectation that we can check against
   return {match=snap(m), nextpos=nextpos, ast=a, input=input, start=start}
end

local function bracket_complement(e, a, input, start, expected, nextpos)
--    print("*** In bracket_complement: a = " .. ast.tostring(a))
--    print("    expected = " .. tostring(expected))
   local dot = e.env:lookup('.')
   if not common.pattern.is(dot) then error("could not find dot pattern in env") end
   dot = assert(dot.peg)
   --- When a bracket expression has its complement flag set, it compiles to
   --- 'dot - cexp' i.e. '. & !cexp' where cexp is the inside of the bracket.
   --- Note that cexp is usually a choice exp, and nowhere in there will we find
   --- the dot.  It's been compiled into the peg of the bracket exp.  (This is
   --- not a good approach, because it makes 'trace' complicated, but we are
   --- refactoring the core of Rosie so that we are not limited by lpeg -- and
   --- rewriting the current code is not worth the effort.  The current code for
   --- 'trace' will be obsolete within a year.)
   local dot_ast = ast.ref.new{localname='.',sourceref=a.sourceref}
   local m, leftover = protected_match(dot_ast,
				       dot,
				       input,
				       start,
				       common.attribute_table_to_table(e.encoder_parms))
   if (expected==false) and (not m) then
      return {match=false, nextpos=nextpos, ast=dot_ast, subs=nil, input=input, start=start}
   end
   if m then nextpos = #input - leftover + 1 end
   local cexp = a.cexp
   local exps
   if ast.choice.is(cexp) then
      exps = cexp.exps
   else
      exps = {cexp}
   end
   local matches = {}
   for _, exp in ipairs(exps) do
      local result = expression(e, exp, input, start)
      result.match = not result.match
--      print("*** *** *** loop: " .. ast.tostring(exp), " expected = "
--	 .. tostring(expected) .. " result.match = " .. tostring(result.match))
      table.insert(matches, result)
      if expected and result.match then break end
      if (not expected) and (not result.match) then break end
   end -- for
   local n = #matches
   if n < #exps then
      append_unattempted(exps, n+1, matches, input, matches[n].nextpos)
   end
   if expected~=nil then
      if (not expected) and matches[n].match then
	 assert(not expected, "bracket_complement match differs from expected")
      elseif expected and (not matches[n].match) then
	 assert(expected, "bracket_complement non-match differs from expected")
      end
   end
   return {match=snap(matches[n].match), nextpos=nextpos, ast=a, subs=matches, input=input, start=start}
end

local function bracket(e, a, input, start, expected, nextpos)
   local result
   if a.complement then
      return bracket_complement(e, a, input, start, expected, nextpos)
   else
      -- not a.complement
      result = expression(e, a.cexp, input, start)
   end
   if expected ~= nil then
      if result.match and (not a.complement) then
	 assert(expected, "bracket match differs from expected" ..
		bracket_explanation(a, input, start, result.match, a.complement))
      elseif (not result.match) and (not a.complement) then
	 assert(not expected, "bracket non-match differs from expected" ..
		bracket_explanation(a, input, start, result.match, a.complement))
      end
   end -- if there is an expectation that we can check against
   return {match=snap(result.match), nextpos=nextpos, ast=a, subs={result}, input=input, start=start}
end
   
local function predicate(e, a, input, start, expected, nextpos)
--   print("***1 predicate a.type = " .. a.type, " expected = " .. tostring(expected))
   if a.type=="lookbehind" then
      return {match=snap(expected), nextpos=nextpos, ast=a, input=input, start=start}
   end
   local result = expression(e, a.exp, input, start)
--   print("***2 predicate a.type = " .. a.type, " expected = " .. tostring(expected))
--   print("*** result.match = " .. tostring(result.match))
   if (result.match and (a.type~="negation")) then
      assert(expected, "predicate match differs from expected (this is a bug)")
      -- Cannot compare nextpos to result.nextpos, because 'a.exp' is NOT a predicate (so it
      -- advances nextpos) whereas 'a' IS a predicate (which does not advance nextpos).
      assert(nextpos==start, "predicate was evaluated, but nextpos advanced ahead of start (this is a bug)")
   elseif ((not result.match) and (a.type~="negation")) then
      assert(not expected,
	     "predicate non-match differs from expected: " .. ast.tostring(a) ..
	     " on input: '" .. input .. "' (start position " .. tostring(start) .. ") (this is a bug)")
   end
   return {match=snap(expected), nextpos=nextpos, ast=a, subs={result}, input=input, start=start}
end

local function and_exp(e, a, input, start, expected, nextpos)
   -- TODO: 
   return {match=snap(expected), nextpos=nextpos, ast=a, input=input, start=start}
end

local function grammar(e, a, input, start, expected, nextpos)
   -- FUTURE: Simulate a grammar using its pieces.  This will require some careful bouncing in and
   -- out of lpeg because we cannot attempt a match against an lpeg.V(rulename) peg.
   return {match=snap(expected), nextpos=nextpos, ast=a, input=input, start=start}
end

local function application(e, a, input, start, expected, nextpos)
   -- FUTURE: Simulate an application by first evaluating its args, then doing
   -- the application itself.
   return {match=snap(expected), nextpos=nextpos, ast=a, input=input, start=start}
end

function expression(e, a, input, start)
   local pat = a.pat
   if not pattern.is(pat) then
      error("Internal error: no pattern stored in ast node " .. ast.tostring(a)
	 .. " (found " .. tostring(pat) .. ")")
   end
   local m, leftover = protected_match(a,
				       pat.peg,
				       input,
				       start,
				       common.attribute_table_to_table(e.encoder_parms))
   m = snap(m)
   local nextpos = (m and #input-leftover+1) or start
   if ast.literal.is(a) then
      return {match=m, nextpos=nextpos, ast=a, input=input, start=start}
   elseif ast.bracket.is(a) then
      return bracket(e, a, input, start, m, nextpos)
   elseif ast.simple_charset_p(a) then
      return cs_simple(e, a, input, start, m, nextpos)
   elseif ast.sequence.is(a) then
      return sequence(e, a, input, start, m, nextpos)
   elseif ast.choice.is(a) then
      return choice(e, a, input, start, m, nextpos)
   elseif ast.and_exp.is(a) then
      return and_exp(e, a, input, start, m, nextpos)
   elseif ast.ref.is(a) then
      return ref(e, a, input, start, m, nextpos)
   elseif ast.atleast.is(a) then
      return atleast(e, a, input, start, m, nextpos)
   elseif ast.atmost.is(a) then
      return atmost(e, a, input, start, m, nextpos)
   elseif ast.predicate.is(a) then
      return predicate(e, a, input, start, m, nextpos)
   elseif ast.grammar.is(a) then
      return grammar(e, a, input, start, m, nextpos)
   elseif ast.application.is(a) then
      return application(e, a, input, start, m, nextpos)
   else
      return table.concat({"Internal error: invalid ast type in trace.expression:" .. tostring(a),
			   "Arguments to trace:",
			   ast.tostring(a),
			   tostring(a),
			   tostring(start),
			   tostring(m),
			   tostring(nextpos)},
			  '\n')
   end
end

function trace.internal(r, input, start)
   start = start or 1
   assert(type(input)=="string")
   assert(type(start)=="number")
   assert(r.pattern and r.engine)		    -- quack
   assert((pcall(rawget, r.pattern, "ast")))	    -- quack: "is ast a valid key in r.pattern?"
   local a = r.pattern.ast
   assert(a, "no ast stored for pattern")
   return expression(r.engine, a, input, start)
end

local function prep_for_export(t)
   if t.ast then
      t.exp = ast.tostring(t.ast)
      t.ast = nil
   end
   if t.match == nil then
      t.match = false
   elseif type(t.match) == "userdata" then
      t.match = true
   end
   if t.subs then
      t.subs = list.map(prep_for_export, t.subs)
   end
   return t
end

-- FUTURE: Make the trace styles more extensible by having a table of them, like we do for match
-- encoders.
function trace.expression(r, input, start, style)
   assert(type(style)=="string")
   local tr = trace.internal(r, input, start)
   if (type(tr) == "string") then
      return common.ERR_NO_PATTERN, tr
   end
   assert(type(tr)=="table")
   local matched = tr.match and true or false
   local retval
   if style == "json" then
      prep_for_export(tr)
      retval = json.encode(tr)
   elseif style == "full" then
      retval = trace.tostring(tr)
   elseif style == "condensed" then
      retval = trace.path_tostring(trace.max_path(tr))
   else
      matched = common.ERR_NO_ENCODER
   end
   return matched, retval
end

return trace


